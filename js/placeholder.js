(function ($) {

Drupal.behaviors.fancyFormAPI.fn.placeholder = {
  attach: function (content, settings) {
    if (settings.fancyFormAPI.placeholder) {
      $.each(settings.fancyFormAPI.placeholder, function(id, text) {
        Drupal.fancyFormAPI.placeholder(id, text);
      });
    }
  }
};

Drupal.fancyFormAPI.placeholder = function(id, text) {
}

})(jQuery);
