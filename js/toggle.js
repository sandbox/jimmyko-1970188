(function ($) {

Drupal.behaviors.fancyFormAPI.fn.toggle = {
  attach: function (context, settings) {
    for (var reactor in settings.fancyFormAPI.toggle) {
      var $reactor = $(reactor, context).addClass('element-invisible');
      var initialize = function() {
        var $trigger = $(settings.fancyFormAPI.toggle[reactor], context)
        $trigger.closest('.form-item').hover(
          function(event) {
            $(this).data('reactor').removeClass('element-invisible');
          },
          function(event) {
            $(this).data('reactor').addClass('element-invisible');
          }
        ).data('reactor', $reactor);
      }
      $reactor.once('toggle', initialize());
    }
  }
}

})(jQuery);
