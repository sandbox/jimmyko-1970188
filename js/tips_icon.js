(function ($) {

Drupal.behaviors.fancyFormAPI.fn.tipsIcon = {
  attach: function (context, settings) {
    $('input.tips-icon', context).closest('.form-item').once('tips-icon', Drupal.tipsIcon);
  }
}

Drupal.tipsIcon = function() {
  if ($('input:checkbox[title]', this).attr('title') !== '') {
    var formItem = $(this)
    var desc = $('.description', formItem).hide();
    var tipsBox = $('<div class="form-tips"><a href="javascript:void(0)">?</a></div>').css('float', 'right')
      .mouseover(function() {
        desc.show().position({
          of: formItem,
          my: 'left top',
          at: 'right top',
          collision: 'flip none',
          offset: '3 0'
        });
        // IE7 fix
        formItem.css('z-index', '1');
      })
      .mouseleave(function() {
        desc.hide();
        // IE7 fix
        formItem.css('z-index', '');
      });

    $(this).prepend(tipsBox);
  }
}

})(jQuery);
