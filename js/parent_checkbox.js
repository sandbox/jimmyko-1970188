(function ($) {

Drupal.behaviors.fancyFormAPI.fn.parentCheckbox = {
  attach: function (context, settings) {
    for (var selector in settings.fancyFormAPI.parentCheckbox) {
      var $context = $(context);
      var $element = $context.find(settings.fancyFormAPI.parentCheckbox[selector]);
      Drupal.parentCheckbox({
        checkboxes: $context.find(selector),
        parent: $element.is(':checkbox') ? $element : $element.find(':checkbox').first()
      });
    }
  }
}

Drupal.parentCheckbox = function(args) {
  // constants
  var CHECKBOX_NONE    = -1
  var CHECKED_NONE    = 0
  var CHECKED_PARTIAL = 1;
  var CHECKED_ALL     = 2;

  var $wrapper = args.checkboxes;
  var $parent = args.parent;

  var cbNumber = $wrapper.find(':checkbox').length;

  var initialize = function() {

    var updateParentCheckbox = function(state) {
      switch(state) {
        case CHECKBOX_NONE:
          if ( !$parent.prop('disabled') ) {
            $parent.prop('indeterminate', false).prop('checked', false).data('ff:LoopParent', true).trigger('change').prop('disabled', true);
          }
          break;
        case CHECKED_NONE:
          $parent.prop('disabled', false);
          if ( $parent.prop('indeterminate') || $parent.prop('checked') ) {
            $parent.prop('indeterminate', false).prop('checked', false).data('ff:LoopParent', true).trigger('change');
          }
          break;
        case CHECKED_PARTIAL:
          $parent.prop('disabled', false);
          if ( !$parent.prop('indeterminate') || $parent.prop('checked') ) {
            $parent.prop('indeterminate', true).prop('checked', false).data('ff:LoopParent', true).trigger('change');
          }
          break;
        case CHECKED_ALL:
          $parent.prop('disabled', false);
          if ( $parent.prop('indeterminate') || !$parent.prop('checked') ) {
            $parent.prop('indeterminate', false).prop('checked', true).data('ff:LoopParent', true).trigger('change');
          }
          break;
      }
    };
    
    var updateCheckboxes = function() {
      var $checkboxes = $wrapper.find(':checkbox');
      $parent.prop('indeterminate', false);
      if ($parent.prop('checked')) {
        $checkboxes.each(function(index, value) {
          if (!$(this).prop('checked')) {
            $(this).prop('checked', true);
          }
        });
      } else {
        $checkboxes.each(function(index, value) {
          if ($(this).prop('checked')) {
            $(this).prop('checked', false);
          }
        });
      }
      if (cbNumber < 50) $wrapper.data('ff:LoopCheckboxes', true).find(':checkbox').trigger('change');
    };

    var getState = function() {
      var $checkboxes = $wrapper.find(':checkbox');
      var $checked = $checkboxes.filter(':checked');
      var hasIndeterminate = false;
      for (var key in $checkboxes) {
        if ($checkboxes.eq(key).prop('indeterminate')) {
          hasIndeterminate = true;
          break;
        }
      }
      if ($checkboxes.length == 0) {
        return CHECKBOX_NONE;
      } else if ($checked.length == 0 && !hasIndeterminate) {
        return CHECKED_NONE;
      } else if ($checkboxes.length == $checked.length) {
        return CHECKED_ALL;
      } else {
        return CHECKED_PARTIAL;
      }
    }

    updateParentCheckbox(getState());

    // checkboxes event
    // We bind event on their wrapper for preventing if there is dom manipulation of
    // chekcboxes.
    $wrapper.on('click change', function (event) {
      // prevent infinite loop of event trigger
      if ($wrapper.data('ff:LoopCheckboxes')) {
        $wrapper.data('ff:LoopCheckboxes', false);
        return;
      }
      updateParentCheckbox(getState());
    });

    // parent checkbox event.
    // We bind click event for fixing the IE bugs: IE never trigger change event if the
    // checkbox is in indeterminate status.
    $parent.on('click change', function (event) {
      // prevent infinite loop of event trigger
      if ($parent.data('ff:LoopParent')) {
        $parent.data('ff:LoopParent', false);
        return;
      }
      updateCheckboxes();
      // trigger this custom event for improving performance
      // we don't trigger change on checkbox input because it consume a lot of
      // computation power wheren this is a long list of checkbox inputs.
      // @see list_checked.js in fancy_form_api
      $wrapper.trigger('checkboxes_change');
    });
  };
  
  $parent.once('parent-checkbox', initialize);
}

})(jQuery);
