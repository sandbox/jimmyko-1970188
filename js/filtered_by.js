(function($) {

Drupal.behaviors.fancyFormAPI.fn.filteredBy = {
  attach: function(context, settings) {
    // create cach namespace
    // // We only cache the data once in the beginning of loading. The filtering
    // data won't be rebuilt after AJAX for performance concern.
    if (Drupal.settings.fancyFormAPI.filteredBy.__cache === undefined) {
      Drupal.settings.fancyFormAPI.filteredBy.__cache = {};
    }

    for (var selector in settings.fancyFormAPI.filteredBy) {
      for (var filter in settings.fancyFormAPI.filteredBy[selector]) {
        var $actor = $(selector, context);
        var $filter = $(filter, context);
        var method = settings.fancyFormAPI.filteredBy[selector][filter];
        Drupal.fancyFormAPI.filteredBy($actor, $filter, method);
      }
    }
  }
}

Drupal.fancyFormAPI.filteredBy = function(actor, filter, method) {

  var initialize = function() {
    var actorType = getFormItemType(actor);
    var filterType = getFormItemType(filter);

    if (filterType == 'text' && actorType == 'checkboxes') {
      var cache = {};
      var regexpPrefix = '';
      var regexpSuffix = '';

      $(actor).find('.form-item').each(function(i, elem) {
        cache[i] = {};
        cache[i].text = $(elem).find('label').text();
        cache[i].elem = $(elem);
      });

      // save as global variable with id
      Drupal.settings.fancyFormAPI.filteredBy.__cache[filter.attr('id')] = cache;


      filter.data('filterMethod', method).on('keyup', function() {
        var pattern = filter.val();
        var modifier = '';
        var cache = Drupal.settings.fancyFormAPI.filteredBy.__cache[filter.attr('id')];
        var methods = method.split(',');
        $.each(methods, function(i, item) {
          switch (item) {
            case 'start with':
              pattern = '^'+pattern;
              break;
            case 'ignore case':
              modifier = 'i'
              break;
          }
        });

        var regexp = new RegExp(pattern, modifier);

        if (pattern === '') {
          $.each(cache, function(i, cacheItem) {
            cacheItem.elem.show();
          });
        } else {
          $.each(cache, function(i, cacheItem) {
            if ($.isArray(cacheItem.text.match(regexp))) {
              cacheItem.elem.show();
            } else {
              cacheItem.elem.hide();
            }
          });
        }
      });
    }

    if (filterType == 'select' && actorType == 'checkboxes' && $.type(method) === "object") {
      var cache = {};
      var members = $();

      // detach all checkboxes first.
      members = actor.find('input:checkbox');
      members.closest('.form-item').detach();

      $.each(method, function(i, values) {
        cache[i] = $();
        $.each(values, function (j, value) {
          var matched = members.filter('[value='+value+']').closest('.form-item');
          cache[i] = cache[i].add(matched);
        });
      });

      // save as global variable with id
      Drupal.settings.fancyFormAPI.filteredBy.__cache[filter.attr('id')] = cache;

      filter.change(function() {
        members.closest('.form-item').detach();

        var selectedValues = $(this).val();
        var cache = Drupal.settings.fancyFormAPI.filteredBy.__cache[filter.attr('id')];
        if (selectedValues != null) {
          $.each(selectedValues, function(i, value) {
            if (cache[value]) {
              actor.append(cache[value]);
            }
          });
        }
        actor.trigger('change');
      });
    }
  }

  var getFormItemType = function(elem) {
    if ($(elem).hasClass('form-checkboxes')) {
      return 'checkboxes';
    }
    if ($(elem).hasClass('form-radios')) {
      return 'radios';
    }
    if ($(elem).hasClass('form-select')) {
      return 'select';
    }
    if ($(elem).hasClass('form-text')) {
      return 'text';
    }
    return '';
  }

  actor.once('filtered-by', initialize);
}

})(jQuery);

