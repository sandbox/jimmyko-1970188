/**
 * @ref tableselect.js
 */
(function ($) {

Drupal.behaviors.fancyFormAPI.fn.selectAll = {
  attach: function (context,settings) {
    var selector = settings.fancyFormAPI.selectAll.join(', ');
    $(selector, context).once('select-all', Drupal.selectAll);
  }
};

Drupal.selectAll = function() {
  // Do not add a "Select all" checkbox if there are no options in the select.
  if ($('option', this).length == 0) {
    return; 
  }

  // Keep track of the select, which select is selected and alias the settings.
  var select = this, options;
  // For each of the options within the select.
  var options = $('option', select);
  var id = $(this).attr('id');
  var strings = {'selectAll': Drupal.t('Select all options in this select list'), 'selectNone': Drupal.t('Deselect all options in this select list') };
  var updateSelectAll = function (state) {
    $(select).parent().find('.select-all').each(function() {
      $(this).attr('title', state ? strings.selectNone : strings.selectAll);
      $(this).find('input:checkbox').prop('checked', state);
    });
  }

  // Find all select with class select-all, and insert check all checkbox.
  selectall = $('<div class="select-all"><input id="'+id+'-select-all" type="checkbox" class="form-checkbox" /> <label class="option" for="'+id+'-select-all">'+Drupal.t('Select all')+'</label></div>').attr('title', strings.selectAll);
  $(select).after(selectall);
  $('input', selectall).click(function (event) {
    if ($(event.target).is('input:checkbox')) {
      // Loop through all options and set their state to the select all checkbox' state.
      options.each(function () {
        $(this).prop('selected', event.target.checked);
      });
      // Update the title and the state of the check all box.
      updateSelectAll(event.target.checked);
      $(select).trigger('change').trigger("liszt:updated");
    }
  });

  $(select).change(function (event) {
    // If all options are selected, make sure the select-all checkbox is checked too, otherwise keep unchecked.
    updateSelectAll((options.length == $(options).filter(':selected').length));
  });
};

})(jQuery);
