(function($) {

Drupal.behaviors.fancyFormAPI.fn.listSelected = {
  attach: function (context, settings) {
    for (var selector in settings.fancyFormAPI.listSelected) {
      var $context = $(context);
      Drupal.listSelected({
        source: $context.find(selector),
        list: $context.find(settings.fancyFormAPI.listSelected[selector])
      });
    }
  }
};

Drupal.listSelected = function(args) {
  // constants
  var STATE_NONE    = 0;
  var STATE_PARTIAL = 1;
  var STATE_ALL     = 2;

  var source = args.source;
  var list = args.list;
  
  var initialize = function() {
    source.each( function() {

      var select = this;
      var lsNone = $('<span>'+Drupal.t('None')+'</span>').addClass('ls-none');
      var lsAll = $('<span>'+Drupal.t('All')+'</span>').addClass('ls-all');

      var options = $(select).find('option').each(function () {
        var option = this;
        var lsItem = $('<span>'+$(option).html()+'</span>').addClass('ls-item').attr('ls-o-value', option.value);
        $(option).data('lsItem', lsItem);
        var lsSeparator = $('<span>, </span>').addClass('ls-separator');
        list.append(lsSeparator).append(lsItem);
      });

      var updateListItem = function(option) {
        if ($(option).prop('selected')) {
          $(option).data('lsItem').show().prev('.ls-separator').show();
        } else {
          $(option).data('lsItem').hide().prev('.ls-separator').hide();
        }
      };

      var updateListSelected = function(state) {
        switch(state) {
          case STATE_NONE:
            clearItems();
            lsAll.hide();
            lsNone.show();
            break;
          case STATE_ALL:
            clearItems();
            lsAll.show();
            lsNone.hide();
            break;
          case STATE_PARTIAL:
            var selected = options.each(function() {
              updateListItem(this);
            }).filter(':selected');
            selected.first().data('lsItem').prev('.ls-separator').hide();
            lsAll.hide();
            lsNone.hide();
            break;
        }
      };

      var getState = function() {
        var selected = $(options).filter(':selected');
        if (selected.length === 0) {
          return STATE_NONE;
        } else if (options.length == selected.length) {
          return STATE_ALL;
        } else {
          return STATE_PARTIAL;
        }
      }

      var clearItems = function() {
        $('.ls-item', list).hide().prev('.ls-separator').hide();
      }

      $(list).append(lsNone).append(lsAll);
      $(select).change(function (event) {
        updateListSelected(getState());
      });

      updateListSelected(getState());
    });
  };

  list.once('list-selected', initialize);
}
  
})(jQuery);
