(function ($) {

Drupal.behaviors.fancyFormAPI.fn.listChecked = {
  attach: function (context, settings) {
    for (var selector in settings.fancyFormAPI.listChecked) {
      var $context = $(context);
      Drupal.listChecked({
        source: $context.find(selector),
        list: $context.find(settings.fancyFormAPI.listChecked[selector])
      });
    }
  }
}

Drupal.listChecked = function(args) {
  // constants
  var STATE_NONE    = 0;
  var STATE_PARTIAL = 1;
  var STATE_ALL     = 2;

  var source = args.source;
  var list = args.list;

  var initialize = function() {
    source.each( function() {

      var lsNone = $('<span>'+Drupal.t('None')+'</span>').addClass('ls-none');
      var lsAll = $('<span>'+Drupal.t('All')+'</span>').addClass('ls-all');

      var $checkboxes = $(this).find(':checkbox').each(function () {
        var $checkbox = $(this);
        var lsItem = $('<span>'+$checkbox.siblings('label').html()+'</span>').addClass('ls-item').attr('ls-cb-value', $checkbox.value);
        $checkbox.data('lsItem', lsItem);
        var lsSeparator = $('<span>, </span>').addClass('ls-separator');
        list.append(lsSeparator).append(lsItem);
      });

      var updateListItem = function(cb) {
        if ($(cb).prop('checked')) {
          $(cb).data('lsItem').show().prev('.ls-separator').show();
        } else {
          $(cb).data('lsItem').hide().prev('.ls-separator').hide();
        }
      };

      var updateListChecked = function(state) {
        switch(state) {
          case STATE_NONE:
            clearItems();
            lsAll.hide();
            lsNone.show();
            list.removeClass('list-checked-all').addClass('list-checked-none');
            break;
          case STATE_ALL:
            clearItems();
            lsAll.show();
            lsNone.hide();
            list.removeClass('list-checked-none').addClass('list-checked-all');
            break;
          case STATE_PARTIAL:
            var $checked = $checkboxes.each(function() {
              updateListItem(this);
            }).filter(':checked');
            $checked.first().data('lsItem').prev('.ls-separator').hide();
            lsAll.hide();
            lsNone.hide();
            list.removeClass('list-checked-none list-checked-all');
            break;
        }
        // custom event to acknowledge the list is updated.
        list.trigger('list_update');
      };

      var getState = function() {
        var $checked = $checkboxes.filter(':checked');
        if ($checked.length === 0) {
          return STATE_NONE;
        } else if ($checkboxes.length == $checked.length) {
          return STATE_ALL;
        } else {
          return STATE_PARTIAL;
        }
      }

      var clearItems = function() {
        $('.ls-item', list).hide().prev('.ls-separator').hide();
      }

      list.append(lsNone).append(lsAll);

      $checkboxes.change(function (event) {
        updateListChecked(getState());
      });
      $checkboxes.closest('.list-checked').bind('checkboxes_change', function (event) {
        updateListChecked(getState());
      });

      updateListChecked(getState());
    });
  }

  list.once('list-checked', initialize);
}

})(jQuery);
