(function($) {

Drupal.behaviors.fancyFormAPI = {
  attach: function (context,settings) {
    for (key in Drupal.behaviors.fancyFormAPI.fn) {
      var ns = Drupal.behaviors.fancyFormAPI.fn[key];
      ns.attach(context,settings);
    }
  }
};

// create behaviors namespace
Drupal.behaviors.fancyFormAPI.fn = {};

// create namespace for fancy functions.
Drupal.fancyFormAPI = {};

})(jQuery);
